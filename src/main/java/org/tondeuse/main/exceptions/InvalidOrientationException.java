package org.tondeuse.main.exceptions;

public class InvalidOrientationException extends Exception {
    public InvalidOrientationException(String message) {
        super(message);
    }
}


package org.tondeuse.main.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Util {
    public static List<String> getFileContent(String filePath) {

            List<String> fileLines = new ArrayList<> ();
        if (filePath != null) {
            try {
                File file = new File (filePath);
                if (!file.exists()) {
                    throw new FileNotFoundException ("File not exist!");
                }

                FileReader fr = new FileReader (file);

                BufferedReader br = new BufferedReader (fr);
                String line = br.readLine ();
                while (line != null) {
                    fileLines.add (line.trim ());
                    line = br.readLine ();
                }
                fr.close ();
            } catch (IOException e) {
                e.printStackTrace ();
            }
        }
            return fileLines;

    }
}

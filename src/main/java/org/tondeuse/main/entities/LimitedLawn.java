package org.tondeuse.main.entities;

public class Lawn {
    private int nbrLine;
    private int nbrColumn;

    public Lawn (int nbrLine, int nbrColumn) {
        this.nbrLine = nbrLine;
        this.nbrColumn = nbrColumn;
    }

    public int getNbrLine() {
        return nbrLine;
    }

    public void setNbrLigne(int nbrLigne) {
        this.nbrLine = nbrLine;
    }

    public int getNbrColumn () {
        return nbrColumn;
    }

    public void setNbrColumn (int nbrColumn) {
        this.nbrColumn = nbrColumn;
    }

    public boolean isContinue(int positionX, int positionY) {
        return (positionX >= 0 && positionX <= nbrColumn) && (positionY >= 0 && positionY <= nbrLine);
    }
}

package org.tondeuse.main.entities;

public final class Movement {
    public static final char ADVANCE = 'A';
    public static final char LEFT = 'G';
    public static final char RIGHT = 'D';
}

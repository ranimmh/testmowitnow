package org.tondeuse.main.entities;

import org.tondeuse.main.exceptions.InvalidOrientationException;

public class LawnMower {
    private int positionX;
    private int positionY;
    private char orientation;

    public LawnMower(int positionX, int positionY, char orientation) throws InvalidOrientationException {
        this.positionX = positionX;
        this.positionY = positionY;
        if (!"NESW".contains(Character.toString(orientation))) {
            throw new InvalidOrientationException (String.format("Orientation not exist", orientation));
        }
        this.orientation = orientation;
    }

    public int getPositionX () {
        return positionX;
    }

    public void setPositionX (int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY () {
        return positionY;
    }

    public void setPositionY (int positionY) {
        this.positionY = positionY;
    }


    public char getOrientation() {
        return orientation;
    }

    public void setOrientation(char orientation) {
        this.orientation = orientation;
    }

    public void advance() {
        switch (this.orientation) {

            case Direction.EAST:
                this.incrementPositionX ();
                break;
            case Direction.SOUTH:
                this.decrementPositionY ();
                break;
            case Direction.NORTH:
                this.incrementPositionY ();
                break;
            case Direction.WEST:
                this.decrementPositionX ();
                break;
        }
    }

    public void left() {
        switch (this.orientation) {
            case Direction.EAST:
                this.setOrientation(Direction.NORTH);
                break;
            case Direction.SOUTH:
                this.setOrientation(Direction.EAST);
                break;
            case Direction.NORTH:
                this.setOrientation(Direction.WEST);
                break;
            case Direction.WEST:
                this.setOrientation(Direction.SOUTH);
                break;
        }
    }

    public void right() {
        switch (this.orientation) {
            case Direction.EAST:
                this.setOrientation(Direction.SOUTH);
                break;
            case Direction.SOUTH:
                this.setOrientation(Direction.WEST);
                break;
            case Direction.NORTH:
                this.setOrientation(Direction.EAST);
                break;
            case Direction.WEST:
                this.setOrientation(Direction.NORTH);
                break;
        }
    }

    public void incrementPositionX() {
        this.positionX++;
    }

    public void incrementPositionY() {
        this.positionY++;
    }

    public void decrementPositionX() {
        this.positionX--;
    }

    public void decrementPositionY() {
        this.positionY--;
    }

    public String getPosition() {
        return this.positionX + " " + this.positionY + " " + this.orientation;
    }

}

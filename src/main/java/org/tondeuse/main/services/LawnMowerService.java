package org.tondeuse.main.services;

import org.tondeuse.main.Main;
import org.tondeuse.main.entities.Lawn;
import org.tondeuse.main.entities.LawnMower;
import org.tondeuse.main.entities.Movement;
import org.tondeuse.main.utils.Util;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LawnMowerService {
    static Logger logger
            = Logger.getLogger(
            Main.class.getName());
    private final String urlFile;
    public LawnMowerService (String urlFile) {
        this.urlFile = urlFile;
    }

    public void piloteLawn () {
        try {
            List<String> fileContent = Util.getFileContent (urlFile);

            logger.log (Level.INFO, "Number of line : " + fileContent.size ());

            String[] length = fileContent.get (0).split (" ");
            Lawn lawn = new Lawn (Integer.parseInt (length[0]), Integer.parseInt (length[1]));

            for (int i = 1; i < fileContent.size (); i++) {

                String[] initials = fileContent.get (i).split (" ");

                LawnMower lawnMower = new LawnMower (Integer.parseInt (initials[0]), Integer.parseInt (initials[1]), initials[2].charAt (0));
                i++;
                String movements = fileContent.get (i);
                checkMovements (movements, lawnMower, lawn);
                logger.log (Level.INFO, "Final position " +lawnMower.getPosition ());
             //   System.out.println ("Final position " +lawnMower.getPosition ());
            }
        } catch (Exception e) {
            logger.log (Level.SEVERE, "An error occurred while running the program: " + e.getMessage ());
        }

    }
    public static void checkMovements (String movements, LawnMower lawnMower, Lawn lawn){
        for (int k = 0; k < movements.length (); k++) {

            char movement = movements.charAt (k);
            if (movement == Movement.LEFT) {
                lawnMower.left ();
            } else if (movement == Movement.RIGHT) {
                lawnMower.right ();
            } else if (movement == Movement.ADVANCE) {
                lawnMower.advance ();
                if (!lawn.isContinue (lawnMower.getPositionX (), lawnMower.getPositionY ())) {


                    lawnMower.incrementPositionY ();
                }
            }
        }
    }

}


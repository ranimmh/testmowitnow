package org.tondeuse.test;

import org.tondeuse.main.entities.Direction;
import org.tondeuse.main.entities.Lawn;
import org.tondeuse.main.entities.LawnMower;
import org.tondeuse.main.exceptions.InvalidOrientationException;
import org.tondeuse.main.services.LawnMowerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith (JUnit4.class)
public class LawnMowerTest {

    @Test
    public void givenLawnMowerValue() throws InvalidOrientationException {
        Lawn lawn = new Lawn (5, 5);
        LawnMower lawnMower1 = new LawnMower (1, 2, 'N');
        LawnMowerService.checkMovements ("GAGAGAGAA", lawnMower1, lawn);
        assertEquals(Direction.NORTH, lawnMower1.getOrientation ());
        assertEquals("1 3 N", lawnMower1.getPosition ());

        LawnMower lawnMower2 = new LawnMower (3, 3, 'E');
        LawnMowerService.checkMovements ("AADAADADDA", lawnMower2, lawn);
        assertEquals(Direction.EAST, lawnMower2.getOrientation ());
        assertEquals("5 1 E", lawnMower2.getPosition ());
    }
    @Test
    public void givenOrientationNWhenCompareToOrientationSThenNotEquals() throws InvalidOrientationException {
        LawnMower lawnMower1 = new LawnMower (1, 2, 'S');
        LawnMower lawnMower2 = new LawnMower (1, 3, 'W');
        assertNotEquals(lawnMower2.getOrientation (), lawnMower1.getOrientation ());
    }

    @Test(expected = InvalidOrientationException.class)
    public void givenOrientationUnknownWhenInstantiatingThenThrowInvalidOrientationExcetpion() throws InvalidOrientationException{
        LawnMower lawnMower = new LawnMower (1, 2, 'K');
    }
}
